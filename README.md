# Complete Intro to React V8

Brian Holt

## Resources

Curriculum:
https://react-v8.holt.courses/

Code for the course website itself:
https://github.com/btholt/complete-intro-to-react-v8

Code for class:
https://github.com/btholt/citr-v8-project/
