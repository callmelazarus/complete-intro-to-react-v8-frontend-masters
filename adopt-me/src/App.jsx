// import React from 'react'
import { createRoot } from 'react-dom';
import Pet from "./Pet";

// old way to write this, without React installed with Vite
// const App = () => {
//   return React.createElement("div", {}, [
//     React.createElement("h1", {}, "Adopt Me!"),
//     React.createElement(Pet),
//     React.createElement(Pet),
//     React.createElement(Pet),
//   ]);
// };

const App = () => {
  <div>
    <h1>Adopt Me!</h1>
    <Pet name="Lune" animal="dog" breed="Havanese"/>
    <Pet name="pep" animal="bird" breed="parrot"/>
    <Pet name="hao" animal="cat" breed="mixed"/>
  </div>
}

const container = document.getElementById("root");
const root = createRoot(container);
// root.render(React.createElement(App));
root.render(<App />)
