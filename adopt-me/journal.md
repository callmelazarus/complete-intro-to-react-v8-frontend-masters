## Feb

**Today I worked on:**
**Reflection:**
**Lesson:**

---


## Feb 28 - Tuesday

**Today I worked on:**
- continued
- Moving into Core react concepts (JSX)
**Reflection:**
- vite is another build tool alternative to something like webpack. 
- Previously we viewed the HTML file just opening the file
**Lesson:**
webpack, parcel, vite (for small files)
Vite (is using rollup below)

"Write code for maintaing code. Code is anotating how to solve a problem that a computer happens to understand"


## Feb 27, 2023 - Monday

**Today I worked on:**
- started project
**Reflection:**
**Lesson:**
- all components are capitalized
- createElement - method that works behind the scenes to create elements in the
- Data flow is typically 1 way (data going down top down) - makes debugging easier
- package.json stores dependencies
  - Holt installs prettier as a dependency using `npm i -D prettier`, which now will act as a dependency
  - `npm run format` will now format upon running that line. 
  
  - `npm i -D eslint`
  - `flag -D, --save-dev: Package will appear in your devDependencies.`

